import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {IFighter} from "../model/fighter";

export  function showFighterDetailsModal(fighter : IFighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}

function createFighterDetails(fighter : IFighter) {
    const { name } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
    const attackInfoElement = createElement({ tagName: 'p', className: 'fighter-attack' });
    const defenseInfoElement = createElement({ tagName: 'p', className: 'fighter-defense' });
    const healthInfoElement = createElement({ tagName: 'p', className: 'fighter-health' });

    // show fighter name, attack, defense, health, image
    nameElement.innerText = `Name: ${name}`;
    attackInfoElement.innerText = `Attack power: ${fighter.attack}`;
    defenseInfoElement.innerText = `Defense power: ${fighter.defense}`;
    healthInfoElement.innerText = `Health: ${fighter.health}`;

    fighterDetails.append(nameElement);
    fighterDetails.append(attackInfoElement);
    fighterDetails.append(defenseInfoElement);
    fighterDetails.append(healthInfoElement);

    return fighterDetails;
}
