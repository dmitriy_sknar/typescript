import { createElement } from '../helpers/domHelper';
import { IFighter } from "../model/fighter";
import {showModal} from "./modal";

export  function showWinnerModal(fighter : IFighter) {
    const title = 'Winner info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createFighterDetails(fighter : IFighter) {
    const { name } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

    nameElement.innerText = name;
    fighterDetails.append(nameElement);

    return fighterDetails;
}
