import {createFighter} from './fighterView';
import {showFighterDetailsModal} from './modals/fighterDetails';
import {createElement} from './helpers/domHelper';
import {fight} from './fight';
import {showWinnerModal} from './modals/winner';
import {getFighterDetails} from "./services/fightersService";
import {IFighter} from "./model/fighter";

export function createFighters(fighters : Array<IFighter>) : HTMLElement {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map((fighter: IFighter) => createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = createElement({tagName: 'div', className: 'fighters'});

    fightersContainer.append(...fighterElements);

    return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event : MouseEvent, fighter : IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) {
    // get fighter form fightersDetailsCache or use getFighterDetails function
    let info;
    if (fightersDetailsCache.has(fighterId)) {
        info = fightersDetailsCache.get(fighterId);
    } else {
        info = getFighterDetails(fighterId);
        fightersDetailsCache.set(fighterId, info); //promise is set here. Despite it works, is that right?
    }
    return info;
}

function createFightersSelector() {
    const selectedFighters = new Map();

    return async function selectFighterForBattle(event : Event, fighter : IFighter) {
        const fullInfo = await getFighterInfo(fighter._id);

        // if (event.target.checked) {
        if ((<HTMLInputElement>event.target).checked) {
            selectedFighters.set(fighter._id, fullInfo);
        } else {
            selectedFighters.delete(fighter._id);
        }

        if (selectedFighters.size === 2) {
            let fightersIter = selectedFighters.values();
            let firstFighter = <IFighter>fightersIter.next().value;
            let secondFighter = <IFighter>fightersIter.next().value;

            const winner = fight(firstFighter, secondFighter);
            showWinnerModal(winner);
            // selectedFighters.clear();
        }
    }
}
