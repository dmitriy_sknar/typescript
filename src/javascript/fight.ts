import {IFighter} from "./model/fighter";

export function fight(firstFighter: IFighter, secondFighter: IFighter) : IFighter{
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    while (firstFighterHealth >= 0 && secondFighterHealth >= 0) {
        let nextMove = Math.floor(Math.random() * Math.floor(2));
        if (nextMove === 1) {
            secondFighterHealth -= getDamage(firstFighter, secondFighter);
        }
        else {
            firstFighterHealth -= getDamage(secondFighter, firstFighter);
        }
    }
    console.log(`firstFighter remaining health: ${firstFighterHealth}`);
    console.log(`secondFighter remaining health: ${secondFighterHealth}`);
    return firstFighterHealth > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker: IFighter, enemy: IFighter): number {
    // damage = hit - block
    // return damage
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    damage = damage >= 0 ? damage : 0;
    return damage;
}

export function getHitPower(fighter: IFighter): number {
    // return hit power
    // getHitPower, расчет силы удара (размер урона здоровью соперника) по формуле
    // power = attack * criticalHitChance;,
    // где criticalHitChance — случайное число от 1 до 2,
    let criticalHitChance = 1 + Math.random();
    let hitPower = fighter.attack * criticalHitChance;
    return hitPower;
}

export function getBlockPower(fighter: IFighter): number {
    // return block power
    // getBlockPower, якирасчет силы блока (амортизация удара соперника) по формуле
    // power = defense * dodgeChance;,
    // де dodgeChance — случайное число от 1 до 2.
    let dodgeChance = 1 + Math.random();
    let blockPower = fighter.defense * dodgeChance;
    return blockPower;
}
