import { callApi } from '../helpers/apiHelper';
import {IFighter} from "../model/fighter";

export async function getFighters() : Promise<Array<IFighter>>{
    try {
        const endpoint = 'fighters.json';
        const apiResult = (await callApi(endpoint, 'GET')) as Array<IFighter>;

        return apiResult;
    } catch (error) {
        throw error;
    }
}

export async function getFighterDetails(id : string) :  Promise<IFighter>{
    // endpoint - `details/fighter/${id}.json`;
    try {
        const endpoint = `details/fighter/${id}.json`;
        const apiResult = await callApi(endpoint, 'GET') as Promise<IFighter>;

        return apiResult;
    } catch (error) {
        throw error;
    }
}

